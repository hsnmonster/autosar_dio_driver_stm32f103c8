
#define STM32F10X_LD
#include <stm32f10x.h>

#include "../Dio.h"

void init_clock(void);
void delayMS(int delay);

int main () {
	
	init_clock();
	
	/* DIO_CHANNEL_ID_BLUE_PILL_LED Init */
	RCC->APB2ENR |= 0x10;
	GPIOC->CRH &= ~(0xF00000);
	GPIOC->CRH |= 0x300000;
	GPIOC->ODR |= 0x2000;
	
	while (1) {
	
		
		if (Dio_ReadChannel(DIO_CHANNEL_ID_BLUE_PILL_LED)) {
			Dio_WriteChannel(DIO_CHANNEL_ID_BLUE_PILL_LED,STD_LOW);
			delayMS(1000);
		}
		else if (!Dio_ReadChannel(DIO_CHANNEL_ID_BLUE_PILL_LED)) {
			Dio_WriteChannel(DIO_CHANNEL_ID_BLUE_PILL_LED,STD_HIGH);
			delayMS(1000);
		}
		
	}

}

void init_clock(void)
{
    // Conf clock : 16MHz using HSE 8MHz crystal w/ PLL X 2 (8MHz x 9 = 16MHz)
    FLASH->ACR      |= FLASH_ACR_LATENCY_2; // 1. Two wait states, per datasheet
    RCC->CFGR       |= RCC_CFGR_PPRE1_2;    // 2. prescale AHB1 = HCLK/2
    RCC->CR         |= RCC_CR_HSEON;        // 3. enable HSE clock
    while( !(RCC->CR & RCC_CR_HSERDY) );    // 4. wait for the HSEREADY flag
    
    RCC->CFGR       |= RCC_CFGR_PLLSRC;     // 5. set PLL source to HSE
    RCC->CFGR       |= RCC_CFGR_PLLMULL2;   // 6. multiply by 2
    RCC->CR         |= RCC_CR_PLLON;        // 7. enable the PLL
    while( !(RCC->CR & RCC_CR_PLLRDY) );    // 8. wait for the PLLRDY flag
    
    RCC->CFGR       |= RCC_CFGR_SW_PLL;     // 9. set clock source to pll

    while( !(RCC->CFGR & RCC_CFGR_SWS_PLL) );    // 10. wait for PLL as source
}

void delayMS(int delay) {
	
	int i;
	for(; delay>0; delay--) {
		for (i=0;i<1770;i++);
	}
	
}

void SystemInit(void) {}
void SystemCoreClockUpdate(void) {}
