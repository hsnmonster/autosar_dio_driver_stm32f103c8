/**
 ******************************************************************************
 * @file  Dio_Cfg.h
 * @brief This is the header file (Type Definitions) for the pre-compile 
 *        configurations of the Dio module of AUTOSAR r4.3.1.
 * 
 ****************************************************************************** 
 */

#ifndef DIO_CFG_H_
#define DIO_CFG_H_

#include "Std_Types.h" /* non standard dependency */

/**
 * @addtogroup Configuration
 * @{
 */

/**
 * @addtogroup Configuration_Fields
 * @{
 */

/* configuration fields */
#define DIO_DEV_ERROR_DETECT STD_ON

#define DIO_FLIP_CHANNEL_API STD_ON

#define DIO_VERSION_INFO_API STD_OFF

/**
 * @addtogroup Configurable_Constants
 * @{
 */

/* configurable constants */
#define DIO_END_OF_LIST  (-1u)

/**
 * @}
 */

/**
 * @}
 */

/**
 * @addtogroup STM32F103C8_GPIO_PORT_Definitions
 * @{
 */

/* definition of stm32f103c8 gpio ports */
typedef struct {
    volatile uint32 CRL; /**< Port configuration register low */
    volatile uint32 CRH; /**< Port configuration register high */
    volatile uint32 IDR; /**< Port input data register */
    volatile uint32 ODR; /**< Port output data register */
    volatile uint32 BSRR; /**< Port bit set/reset register */
    volatile uint32 BRR; /**< Port bit reset register */
    volatile uint32 LCKR; /**< Port configuration lock register */
} Dio_GpioType;

#define DIO_PORTA_BASE (0x40010800u)
#define DIO_PORTB_BASE (0x40010C00u)
#define DIO_PORTC_BASE (0x40011000u)
#define DIO_PORTD_BASE (0x40011400u)


#undef GPIOA
#define GPIOA ((Dio_GpioType*)DIO_PORTA_BASE)
#undef GPIOB
#define GPIOB ((Dio_GpioType*)DIO_PORTB_BASE)
#undef GPIOC
#define GPIOC ((Dio_GpioType*)DIO_PORTC_BASE)
#undef GPIOD
#define GPIOD ((Dio_GpioType*)DIO_PORTD_BASE)

/**
 * @}
 */

/**
 * @addtogroup Symbolic_Names_Channels_Ports_Groups
 * @{
 */

/* symbolic ids for channels - channel groups - ports */
#define DIO_CHANNEL_ID_BLUE_PILL_LED 0u

#define DIO_CHANNEL_GROUP_ID_INPUT_BUTTONS_PTR &(DioChannelGroupConfigData[0].ChannelGroup)

#define DIO_PORT_ID_INPUT_BUTTONS 1u

#define DIO_PORT_ID_7_SEGMENT 0u

/**
 * @}
 */

/**
 * @addtogroup Types_Physical_Channels_Ports_Groups
 * @{
 */

/* physical structure definitions for channel - channel group - port */
typedef struct {
    Dio_ChannelType ChannelId;
    Dio_GpioType* GpioPortPtr;
    uint8 PinId;
}Dio_PhysicalChannelType;

typedef struct {
    Dio_ChannelGroupType ChannelGroup;
    Dio_GpioType* GpioPortPtr;
}Dio_PhysicalChannelGroupType;

typedef struct {
    Dio_PortType PortId;
    Dio_GpioType* GpioPortPtr;
    uint32 mask; /**< Mask which defines the available bits for use for a DIO port */
}Dio_PhysicalPortType;

/**
 * @}
 */

/**
 * @}
 */

/* maping arrays between ids and physical gpio entities */
extern const Dio_PhysicalChannelType DioChannelConfigData[];

extern const Dio_PhysicalChannelGroupType DioChannelGroupConfigData[];

extern const Dio_PhysicalPortType DioPortConfigData[];

extern const uint8 DioChannelConfigDataLength;

extern const uint8 DioChannelGroupConfigDataLength;

extern const uint8 DioPortConfigDataLength;

#endif
