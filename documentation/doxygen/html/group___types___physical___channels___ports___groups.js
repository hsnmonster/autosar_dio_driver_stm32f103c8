var group___types___physical___channels___ports___groups =
[
    [ "Dio_PhysicalChannelType", "struct_dio___physical_channel_type.html", [
      [ "ChannelId", "struct_dio___physical_channel_type.html#af4ab1bb623b46dbd8b49b9c4a6e27de2", null ],
      [ "GpioPortPtr", "struct_dio___physical_channel_type.html#af8a18ed74f43fba4b925eec66e607b57", null ],
      [ "PinId", "struct_dio___physical_channel_type.html#a66540faa612f118b3750625edd05cf15", null ]
    ] ],
    [ "Dio_PhysicalChannelGroupType", "struct_dio___physical_channel_group_type.html", [
      [ "ChannelGroup", "struct_dio___physical_channel_group_type.html#a8724c6bcbc2d4454ba15ed6ad5196a1e", null ],
      [ "GpioPortPtr", "struct_dio___physical_channel_group_type.html#af8a18ed74f43fba4b925eec66e607b57", null ]
    ] ],
    [ "Dio_PhysicalPortType", "struct_dio___physical_port_type.html", [
      [ "GpioPortPtr", "struct_dio___physical_port_type.html#af8a18ed74f43fba4b925eec66e607b57", null ],
      [ "PortId", "struct_dio___physical_port_type.html#af0b163eec48db0baa0fdf38b71f7a7a6", null ]
    ] ]
];