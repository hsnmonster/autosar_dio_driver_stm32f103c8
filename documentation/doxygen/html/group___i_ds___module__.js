var group___i_ds___module__ =
[
    [ "DIO_FLIPCHANNEL_ID", "group___i_ds___module__.html#gaa5c2eb7fcce7d5bf679745e011c66131", null ],
    [ "DIO_GETVERSIONINFO_ID", "group___i_ds___module__.html#gabd6de2ecca92f1bc0fb85549d2ac3425", null ],
    [ "DIO_ID", "group___i_ds___module__.html#gaf263024c2b7231d17c0025610a61c6b3", null ],
    [ "DIO_READCHANNEL_ID", "group___i_ds___module__.html#gadf4fbcd229335bee594c82a3b039e73d", null ],
    [ "DIO_READCHANNELGROUP_ID", "group___i_ds___module__.html#ga9c662ea0d99adca02eab6ce1f8702ecc", null ],
    [ "DIO_READPORT_ID", "group___i_ds___module__.html#ga7bf577c86dcecea126cea492657a9fc2", null ],
    [ "DIO_WRITECHANNEL_ID", "group___i_ds___module__.html#gad922246b77d32633ac8f93d6faebb4cc", null ],
    [ "DIO_WRITECHANNELGROUP_ID", "group___i_ds___module__.html#ga93d51491d0fdbe1c6741da9ceba4a016", null ],
    [ "DIO_WRITEPORT_ID", "group___i_ds___module__.html#gac7e4d752271536c545c02598f4d6335a", null ]
];