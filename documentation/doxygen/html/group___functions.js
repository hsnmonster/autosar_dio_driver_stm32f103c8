var group___functions =
[
    [ "Dio_FlipChannel", "group___functions.html#ga16650b42ff331c4df37e50a3c99c03e1", null ],
    [ "Dio_GetVersionInfo", "group___functions.html#ga5d066e152694e47a84cb22fa73c54c18", null ],
    [ "Dio_ReadChannel", "group___functions.html#gaa5482dd0b4b8e3cd7c984cffbc423330", null ],
    [ "Dio_ReadChannelGroup", "group___functions.html#gadbfa78d0f8a1bf3defe1b89a916663c5", null ],
    [ "Dio_ReadPort", "group___functions.html#ga9d1cb52352ab6ced4a470b5b4e817c92", null ],
    [ "Dio_WriteChannel", "group___functions.html#gab5069dd14692cf83b1a90d3f98faf158", null ],
    [ "Dio_WriteChannelGroup", "group___functions.html#ga5c5c80a98fa1db5d09e0fa30fef1f919", null ],
    [ "Dio_WritePort", "group___functions.html#ga3f2328a3c8a1e4aab0ec3cd4ba1fd241", null ]
];