var group___a_t_mega328p___g_p_i_o___p_o_r_t___definitions =
[
    [ "Dio_GpioType", "struct_dio___gpio_type.html", [
      [ "BRR", "struct_dio___gpio_type.html#aa8d3a0c890d3f9c89da91ebffe8b0027", null ],
      [ "BSRR", "struct_dio___gpio_type.html#a9b5ad0e1100cfde5cef2defd86244232", null ],
      [ "CRH", "struct_dio___gpio_type.html#a92070f32d18d7e93d551ea0bcb14f533", null ],
      [ "CRL", "struct_dio___gpio_type.html#a5a19bbf59a121b5b070ee90dab9321dc", null ],
      [ "IDR", "struct_dio___gpio_type.html#a8e4a8a3259ca29434ab1c1b58e169f6b", null ],
      [ "LCKR", "struct_dio___gpio_type.html#a1b94b869d0b01355b5476c3f41170fc8", null ],
      [ "ODR", "struct_dio___gpio_type.html#a5ed6fe7400eff1c36063c83c83869521", null ]
    ] ],
    [ "DIO_PORTA_BASE", "group___a_t_mega328p___g_p_i_o___p_o_r_t___definitions.html#ga08a4f2b4827e6f83c9c367c2373fc07b", null ],
    [ "DIO_PORTB_BASE", "group___a_t_mega328p___g_p_i_o___p_o_r_t___definitions.html#ga236b976d609b74e2a8a8dab57b632ebb", null ],
    [ "DIO_PORTC_BASE", "group___a_t_mega328p___g_p_i_o___p_o_r_t___definitions.html#gaf3445935f0ad2e9c86f0e5d3c3281536", null ],
    [ "DIO_PORTD_BASE", "group___a_t_mega328p___g_p_i_o___p_o_r_t___definitions.html#gad29f52d8073adaf7ab0656766be9969f", null ],
    [ "GPIOA", "group___a_t_mega328p___g_p_i_o___p_o_r_t___definitions.html#gac485358099728ddae050db37924dd6b7", null ],
    [ "GPIOB", "group___a_t_mega328p___g_p_i_o___p_o_r_t___definitions.html#ga68b66ac73be4c836db878a42e1fea3cd", null ],
    [ "GPIOC", "group___a_t_mega328p___g_p_i_o___p_o_r_t___definitions.html#ga2dca03332d620196ba943bc2346eaa08", null ],
    [ "GPIOD", "group___a_t_mega328p___g_p_i_o___p_o_r_t___definitions.html#ga7580b1a929ea9df59725ba9c18eba6ac", null ]
];