# README #

This README would normally document whatever steps are necessary to get your application up and running.

#### Note:####
For a more comprehensive documentation check: documentation/doxygen/html/index.html

### What is this repository for? ###

* This is a public repository which includes an implementation for the DIO Module of the AUTOSAR r4.3.1 on STM32F103C8 which is included - by default - within the STM32 Blue Pill board. This work is considered as an assignment/practice for the course of "Introduction to AUTOSAR" provided by Ramp Up Academy.

### How do I get set up? ###

* This work has been compiled and tested using KEIL uVision 4 (MDK-ARM) and burned using STM32 Flash Loader Demonstrator.
