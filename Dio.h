/**
 ******************************************************************************
 * @file  Dio.h
 * @brief This is the header file (Type Definitions + API Declarations) of
 *        the Dio module of AUTOSAR r4.3.1.
 * 
 ****************************************************************************** 
 */

#ifndef DIO_H_
#define DIO_H_

#define DIO_CONFIG_HEADER_INCLUDE_LOCATION 0 /* non autosar macro needed to adjust inclusion location for Dio_Cfg.h */

#include "Std_Types.h"

#if (DIO_CONFIG_HEADER_INCLUDE_LOCATION == 1)
#include "Dio_Cfg.h"
#endif

/**
 * @addtogroup Versioning_Macros
 * @{
 */

/* virtual values for versioning */
#define DIO_VENDOR_ID 0x55u
#define DIO_SW_MAJOR_VERSION 0x02u
#define DIO_SW_MINOR_VERSION 0x01u
#define DIO_SW_PATCH_VERSION 0x03u

/**
 * @}
 */

/**
 * @addtogroup IDs_Module_Functions
 * @{
 */

/* api ids */
#define DIO_READCHANNEL_ID 0x00u
#define DIO_WRITECHANNEL_ID 0x01u
#define DIO_READPORT_ID 0x02u
#define DIO_WRITEPORT_ID 0x03u
#define DIO_READCHANNELGROUP_ID 0x04u
#define DIO_WRITECHANNELGROUP_ID 0x05u
#define DIO_GETVERSIONINFO_ID 0x12u
#define DIO_FLIPCHANNEL_ID 0x11u

/* module id */
#define DIO_ID 0x78u /**< Module ID */

/**
 * @}
 */

/**
 * @addtogroup Error_IDs
 * @{
 */

/* error ids */
#define DIO_E_PARAM_INVALID_CHANNEL_ID 0x0Au
#define DIO_E_PARAM_INVALID_PORT_ID 0x14u
#define DIO_E_PARAM_INVALID_GROUP 0x1Fu
#define DIO_E_PARAM_POINTER 0x20u

/**
 * @}
 */

/**
 * @addtogroup Macro_Functions
 * @{
 */

/* non-autosar macro functions */
#define DIO_ENABLE_INTERRUPTS() __asm volatile ("cpsie i")
#define DIO_DISABLE_INTERRUPTS() __asm volatile ("cpsid i")

/**
 * @}
 */

/**
 * @addtogroup Types
 * @{
 */

/* type definitions */
typedef uint8 Dio_ChannelType; /**< Numeric ID of a DIO channel */
typedef uint8 Dio_PortType; /**< Numeric ID of a DIO port */
/** 
 * @brief Definition of a channel group, which consists of several adjoining channels within a port
 */
typedef struct {
    uint32 mask; /**< Mask which defines the positions of the channel group */
    uint8 offset; /**< the position of the Channel Group on the port, counted from the LSB */
    Dio_PortType port; /**< The port on which the channel group is defined */
}Dio_ChannelGroupType;
typedef uint8 Dio_LevelType; /**< The possible levels a DIO channel can have: STD_HIGH or STD_LOW */
typedef uint32 Dio_PortLevelType; /**< The aggregate levels of a DIO port */

/**
 * @}
 */

#if (DIO_CONFIG_HEADER_INCLUDE_LOCATION == 0)
#include "Dio_Cfg.h"
#endif

/* function definitions */
Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId);
void Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level);
Dio_PortLevelType Dio_ReadPort(Dio_PortType PortId);
void Dio_WritePort(Dio_PortType PortId, Dio_PortLevelType Level);
Dio_PortLevelType Dio_ReadChannelGroup(const Dio_ChannelGroupType* ChannelGroupIdPtr);
void Dio_WriteChannelGroup(const Dio_ChannelGroupType* ChannelGroupIdPtr,Dio_PortLevelType Level);

#if (DIO_VERSION_INFO_API == STD_ON)
void Dio_GetVersionInfo(Std_VersionInfoType* VersionInfo);
#endif

#if (DIO_FLIP_CHANNEL_API == STD_ON)
Dio_LevelType Dio_FlipChannel(Dio_ChannelType ChannelId);
#endif

#endif
