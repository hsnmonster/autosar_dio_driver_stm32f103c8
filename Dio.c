/**
 ******************************************************************************
 * @file  Dio.c
 * @brief This is the source file (API Definitions) of
 *        the Dio module of AUTOSAR r4.3.1.
 * 
 ****************************************************************************** 
 */

#include "Dio.h"
#include "Det.h"

/**
 * @addtogroup Functions
 * @{
 */

/** 
 * @brief Returns the value of the specified DIO channel
 * @par ID
 * 0x00
 * @par Sync/Async
 * Synchronous
 * @par Reentrancy
 * Reentrant
 * @param[in] ChannelId ID of DIO channel
 * @param[in,out] None
 * @param[out] None
 * @return The level of the specified DIO channel
 * @see Dio_WriteChannel(), Dio_FlipChannel(), Dio_ReadPort(), Dio_ReadChannelGroup()
 */
Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId) {

    /* determine the channel */
    uint8 counter = 0;
    while ((DioChannelConfigData[counter].ChannelId != (uint8)DIO_END_OF_LIST) && (counter < DioChannelConfigDataLength)) {
        if (DioChannelConfigData[counter].ChannelId == ChannelId) break;
        ++counter;
    }

    /* det work */
    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    if ((DioChannelConfigData[counter].ChannelId == (uint8)DIO_END_OF_LIST) || (counter == DioChannelConfigDataLength)) {
        Det_ReportError(DIO_ID, 0, DIO_READCHANNEL_ID, DIO_E_PARAM_INVALID_CHANNEL_ID);
        return 0;
    }
    #endif

    /* read channel level */
    DIO_DISABLE_INTERRUPTS();
    if (DioChannelConfigData[counter].GpioPortPtr->IDR & (1<<DioChannelConfigData[counter].PinId)) {
        DIO_ENABLE_INTERRUPTS();
        return STD_HIGH;
    }
    else {
        DIO_ENABLE_INTERRUPTS();
        return STD_LOW;
    }
}

/** 
 * @brief Service to set a level of a channel
 * @par ID
 * 0x01
 * @par Sync/Async
 * Synchronous
 * @par Reentrancy
 * Reentrant
 * @param[in] ChannelId ID of DIO channel
 * @param[in] Level Value to be written
 * @param[in,out] None
 * @param[out] None
 * @return None
 * @see Dio_ReadChannel(), Dio_FlipChannel(), Dio_WritePort(), Dio_WriteChannelGroup()
 */
void Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level) {
    
    /* determine the channel */
    uint8 counter = 0;
    while ((DioChannelConfigData[counter].ChannelId != (uint8)DIO_END_OF_LIST) && (counter < DioChannelConfigDataLength)) {
        if (DioChannelConfigData[counter].ChannelId == ChannelId) break;
        ++counter;
    }

    /* det work */
    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    if (DioChannelConfigData[counter].ChannelId == (uint8)DIO_END_OF_LIST) {
        Det_ReportError(DIO_ID, 0, DIO_WRITECHANNEL_ID, DIO_E_PARAM_INVALID_CHANNEL_ID);
    }
    else {
    #endif

    /* write channel level */
    if (Level == STD_HIGH) {
        DIO_DISABLE_INTERRUPTS();
        DioChannelConfigData[counter].GpioPortPtr->ODR |= (1<<(DioChannelConfigData[counter].PinId));
        DIO_ENABLE_INTERRUPTS();
    }
    else {
        DIO_DISABLE_INTERRUPTS();
        DioChannelConfigData[counter].GpioPortPtr->ODR &= ~(1<<(DioChannelConfigData[counter].PinId));
        DIO_ENABLE_INTERRUPTS();
    }

    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    }
    #endif
}

/** 
 * @brief Returns the level of all channels of that port
 * @par ID
 * 0x02
 * @par Sync/Async
 * Synchronous
 * @par Reentrancy
 * Reentrant
 * @param[in] PortId ID of DIO Port
 * @param[in,out] None
 * @param[out] None
 * @return Level of all channels of that port
 * @see Dio_WritePort(), Dio_ReadChannel(), Dio_ReadChannelGroup()
 */
Dio_PortLevelType Dio_ReadPort(Dio_PortType PortId) {

    Dio_PortLevelType PortValue; /* out parameter */

    /* determine the port */
    uint8 counter = 0;
    while ((DioPortConfigData[counter].PortId != (uint8)DIO_END_OF_LIST) && (counter < DioPortConfigDataLength)) {
        if (DioPortConfigData[counter].PortId == PortId) break;
        ++counter;
    }

    /* det work */
    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    if ((DioPortConfigData[counter].PortId == (uint8)DIO_END_OF_LIST) || (counter == DioPortConfigDataLength) ) {
        Det_ReportError(DIO_ID, 0, DIO_READPORT_ID, DIO_E_PARAM_INVALID_PORT_ID);
        return 0;
    }
    #endif

    /* read port value */
    DIO_DISABLE_INTERRUPTS();
    PortValue = (DioPortConfigData[counter].GpioPortPtr->IDR);
    DIO_ENABLE_INTERRUPTS();

    return PortValue;
}

/** 
 * @brief Service to set a value of the port
 * @par ID
 * 0x03
 * @par Sync/Async
 * Synchronous
 * @par Reentrancy
 * Reentrant
 * @param[in] PortId ID of DIO Port
 * @param[in] Level Value to be written
 * @param[in,out] None
 * @param[out] None
 * @return None
 * @see Dio_ReadPort(), Dio_WriteChannel(), Dio_WriteChannelGroup()
 */
void Dio_WritePort(Dio_PortType PortId, Dio_PortLevelType Level) {

    /* determine the port */
    uint8 counter = 0;
    while ((DioPortConfigData[counter].PortId != (uint8)DIO_END_OF_LIST) && (counter < DioPortConfigDataLength)) {
        if (DioPortConfigData[counter].PortId == PortId) break;
        ++counter;
    }

    /* det work */
    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    if (DioPortConfigData[counter].PortId == (uint8)DIO_END_OF_LIST) {
        Det_ReportError(DIO_ID, 0, DIO_WRITEPORT_ID, DIO_E_PARAM_INVALID_PORT_ID);
    }
    else {
    #endif

    /* write port value */
    DIO_DISABLE_INTERRUPTS();
    DioPortConfigData[counter].GpioPortPtr->ODR = Level;
    DIO_ENABLE_INTERRUPTS();

    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    }
    #endif
}

/** 
 * @brief This Service reads a subset of the adjoining bits of a port
 * @par ID
 * 0x04
 * @par Sync/Async
 * Synchronous
 * @par Reentrancy
 * Reentrant
 * @param[in] ChannelGroupIdPtr Pointer to Channel Group
 * @param[in,out] None
 * @param[out] None
 * @return The level of a subset of the adjoining bits of a port
 * @see Dio_WriteChannelGroup(), Dio_ReadChannel(), Dio_ReadPort()
 */
Dio_PortLevelType Dio_ReadChannelGroup(const Dio_ChannelGroupType* ChannelGroupIdPtr) {

    Dio_PortLevelType ChannelGroupValue; /* out parameter */

    /* determine the channel group */
    uint8 counter = 0;
    while ((DioChannelGroupConfigData[counter].ChannelGroup.port != (uint8)DIO_END_OF_LIST) && (counter < DioChannelGroupConfigDataLength)) {
        if (DioChannelGroupConfigData[counter].ChannelGroup.port == ChannelGroupIdPtr->port) break;
        ++counter;
    }

    /* det work */
    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    if ((DioChannelGroupConfigData[counter].ChannelGroup.port == (uint8)DIO_END_OF_LIST) || (counter == DioChannelGroupConfigDataLength)) {
        Det_ReportError(DIO_ID, 0, DIO_READCHANNELGROUP_ID, DIO_E_PARAM_INVALID_GROUP);
        return 0;
    }
    #endif

    /* read channel group value */
    DIO_DISABLE_INTERRUPTS();
    ChannelGroupValue = (((DioChannelGroupConfigData[counter].GpioPortPtr->IDR) & (ChannelGroupIdPtr->mask))>>(ChannelGroupIdPtr->offset));
    DIO_ENABLE_INTERRUPTS();

    return ChannelGroupValue;
}

/** 
 * @brief Service to set a subset of the adjoining bits of a port to a specified level
 * @par ID
 * 0x05
 * @par Sync/Async
 * Synchronous
 * @par Reentrancy
 * Reentrant
 * @param[in] ChannelGroupIdPtr Pointer to ChannelGroup
 * @param[in] Level Value to be written
 * @param[in,out] None
 * @param[out] None
 * @return None
 * @see Dio_ReadChannelGroup(), Dio_WriteChannel(), Dio_WritePort()
 */
void Dio_WriteChannelGroup(const Dio_ChannelGroupType* ChannelGroupIdPtr,Dio_PortLevelType Level) {

    /* determine the channel group */
    uint8 counter = 0;
    while ((DioChannelGroupConfigData[counter].ChannelGroup.port != (uint8)DIO_END_OF_LIST) && (counter < DioChannelGroupConfigDataLength)) {
        if (DioChannelGroupConfigData[counter].ChannelGroup.port == ChannelGroupIdPtr->port) break;
        ++counter;
    }

    /* det work */
    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    if (DioChannelGroupConfigData[counter].ChannelGroup.port == (uint8)DIO_END_OF_LIST) {
        Det_ReportError(DIO_ID, 0, DIO_WRITECHANNELGROUP_ID, DIO_E_PARAM_INVALID_GROUP);
    }
    else {
    #endif
    
    /* write channel group value */
    DIO_DISABLE_INTERRUPTS();
    DioChannelGroupConfigData[counter].GpioPortPtr->ODR = (((DioChannelGroupConfigData[counter].GpioPortPtr->ODR) & ~(ChannelGroupIdPtr->mask)) | ((Level << (ChannelGroupIdPtr->offset)) & (ChannelGroupIdPtr->mask)));
    DIO_ENABLE_INTERRUPTS();
    
    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    }
    #endif
}

/** 
 * @brief Service to get the version information of this module
 * @par ID
 * 0x12
 * @par Sync/Async
 * Synchronous
 * @par Reentrancy
 * Reentrant
 * @param[in] None
 * @param[in,out] None
 * @param[out] VersionInfo Pointer to where to store the version information of this module
 * @return None
 * @see None
 * @note To use this function, the macro DIO_VERSION_INFO_API has to be defined as STD_ON
 */
void Dio_GetVersionInfo(Std_VersionInfoType* VersionInfo) {

    /* det work */
    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    if (VersionInfo == (void*)0) {
        Det_ReportError(DIO_ID,0,DIO_GETVERSIONINFO_ID,DIO_E_PARAM_POINTER);
    }
    else {
    #endif

    /* provide the needed data for the out parameter */
    VersionInfo->moduleID = DIO_ID;
    VersionInfo->vendorID = DIO_VENDOR_ID;
    VersionInfo->sw_major_version = DIO_SW_MAJOR_VERSION;
    VersionInfo->sw_minor_version = DIO_SW_MINOR_VERSION;
    VersionInfo->sw_patch_version = DIO_SW_PATCH_VERSION;

    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    }
    #endif
}

/** 
 * @brief Service to flip the level of a channel and return the level of the channel after flip
 * @par ID
 * 0x11
 * @par Sync/Async
 * Synchronous
 * @par Reentrancy
 * Reentrant
 * @param[in] ChannelId ID of DIO channel
 * @param[in,out] None
 * @param[out] None
 * @return The level of the channel after flip
 * @see Dio_WriteChannel(), Dio_ReadChannel()
 * @note To use this function, the macro DIO_FLIP_CHANNEL_API has to be defined as STD_ON
 */
Dio_LevelType Dio_FlipChannel(Dio_ChannelType ChannelId) {

    Dio_LevelType ChannelValue; /* out parameter */
    uint32 DirectionCheck;

    /* determine the channel */
    uint8 counter = 0;
    while ((DioChannelConfigData[counter].ChannelId != (uint8)DIO_END_OF_LIST) && (counter < DioChannelGroupConfigDataLength)) {
        if (DioChannelConfigData[counter].ChannelId == ChannelId) break;
        ++counter;
    }

    /* det work */
    #if (DIO_DEV_ERROR_DETECT == STD_ON)
    if ((DioChannelConfigData[counter].ChannelId == (uint8)DIO_END_OF_LIST) && (counter < DioChannelGroupConfigDataLength)) {
        Det_ReportError(DIO_ID, 0, DIO_FLIPCHANNEL_ID, DIO_E_PARAM_INVALID_CHANNEL_ID);
        return 0;
    }
    #endif

    /* read current channel level */
    ChannelValue = (((DioChannelConfigData[counter].GpioPortPtr->IDR) & (1<<(DioChannelConfigData[counter].PinId)))>>(DioChannelConfigData[counter].PinId));

    /* write channel level */
    DIO_DISABLE_INTERRUPTS();
    if (DioChannelConfigData[counter].PinId < 8) {
        DirectionCheck = ((DioChannelConfigData[counter].GpioPortPtr->CRL) & (((uint32)0x3)<<((DioChannelConfigData[counter].PinId)*4)));
    }
    else {
        DirectionCheck = ((DioChannelConfigData[counter].GpioPortPtr->CRH) & (((uint32)0x3)<<(((DioChannelConfigData[counter].PinId)-8)*4)));
    }
		
    if (DirectionCheck) {
        DioChannelConfigData[counter].GpioPortPtr->ODR ^=  (1<<(DioChannelConfigData[counter].PinId));
        DIO_ENABLE_INTERRUPTS();

        if (!ChannelValue) {
            return STD_HIGH;
        }
        else {
            return STD_LOW;
        }
    }
    else {
        DIO_ENABLE_INTERRUPTS();
        if (ChannelValue) {
            return STD_HIGH;
        }
        else {
            return STD_LOW;
        }
    }

}

/**
 * @}
 */
